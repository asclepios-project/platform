# asclepios-platform-docker-compose


### Run

* First you need to do some set up.
  (edit the file `.env` to configure the deployment)

```command
cp .env.example .env
sed -i -e "s|192.168.178.108|<YOUR_IP_ADDRESS_HERE>|g" .env
docker-compose -f docker-compose.bootstrap.yml up
```

* Run the stack

```command
docker-compose up -d
```

### Set up minio

* Go to `http://<YOUR_IP_ADDDRESS_HERE>:9000` and login to minio with the credentials in your `.env` file

